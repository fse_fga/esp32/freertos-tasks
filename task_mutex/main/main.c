#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_log.h"

SemaphoreHandle_t mutexI2C;
float temperatura;

float acessa_i2c(int comando)
{
  if (comando == 1)
  {
    ESP_LOGI("I2C", "Leitura de temperatura");
    return 20.0 + ((float)rand() / (float)(RAND_MAX / 10));
  }
  else
  {
    ESP_LOGI("I2C", "Escrita no LCD");
    printf("TELA LCD: Temperatura Ambinete = %f\n", temperatura);
  }
  return 0;
}

void le_sensor(void *params)
{
  while (true)
  {
    if (xSemaphoreTake(mutexI2C, 1000 / portTICK_PERIOD_MS))
    {
      temperatura = acessa_i2c(1);
      ESP_LOGI("Le Sensor", "Temperatura: %f\n", temperatura);
      xSemaphoreGive(mutexI2C);
    }
    else
    {
      ESP_LOGE("Leitura Sensor", "Time out na leitura");
    }
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
}

void lcd_display(void *params)
{
  while (true)
  {
    if (xSemaphoreTake(mutexI2C, 1000 / portTICK_PERIOD_MS))
    {
      ESP_LOGI("Display", "Escreve no display: %f\n", temperatura);
      acessa_i2c(2);
      xSemaphoreGive(mutexI2C);
    }
    else
    {
      ESP_LOGE("LCD", "Timeout escrita do LCD");
    }
    vTaskDelay(5000 / portTICK_PERIOD_MS);
  }
}

void app_main(void)
{
  mutexI2C = xSemaphoreCreateMutex();

  xTaskCreate(&le_sensor, "Leitura Sensor 1", 2048, NULL, 2, NULL);
  xTaskCreate(&lcd_display, "Atualização do Display", 2048, NULL, 2, NULL);
}