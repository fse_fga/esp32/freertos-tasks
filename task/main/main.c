#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"

const char *TAG = "Main";

void task1(void * params)
{
  while (true)
  {
    printf("Leitura de sensores: %s\n", (char *) params);
    // ESP_LOGI("Task 1", "Leitura dos sensores: %s", (char *) params);
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
}

void task2(void * params) 
{
  while (true)
  {
    printf("Escreve no display: %s\n", (char *) params);
    // ESP_LOGI("Task 2", "Escreve no display: %s", (char *) params);
    vTaskDelay(5000 / portTICK_PERIOD_MS);
  }
}

void app_main(void)
{
  xTaskCreate(&task1, "Leitura de Sensores", 2048, "task 1", 2, NULL);
  xTaskCreate(&task2, "Atualização do Display", 2048, "task 2", 2, NULL);
  //xTaskCreatePinnedToCore(&task2, "Atualização do Display", 2048, "task 2", 2, NULL, 0); // Core 0 ou Core 1

  // ESP_LOGE(TAG, "This is an error");
  // ESP_LOGW(TAG, "This is a warning");
  // ESP_LOGI(TAG, "This is an info");
  // ESP_LOGD(TAG, "This is a Debug");
  // ESP_LOGV(TAG, "This is Verbose");
}