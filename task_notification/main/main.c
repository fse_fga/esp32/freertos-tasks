#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

static TaskHandle_t receptorHandler = NULL;

void emissor(void *params)
{
  while (true)
  {
    xTaskNotifyGive(receptorHandler);
    vTaskDelay(2000 / portTICK_PERIOD_MS);
  }
}

void receptor(void *params)
{
  while (true)
  {
    int quantidade_de_notificacoes = ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
    printf("Notificações Recebidas %d\n", quantidade_de_notificacoes);
  }
}

void app_main(void)
{
  xTaskCreate(&receptor, "Receptor", 2048, NULL, 2, &receptorHandler);
  xTaskCreate(&emissor, "Emissor", 2048, NULL, 2, NULL);
  xTaskCreate(&emissor, "Emissor 2", 2048, NULL, 2, NULL);
}